---
layout: post
title: "semantic compasses"
date:   2020-01-01 17:41:39 -0700
text: default
links:
  - text: live demo (coming soon)
    url: 
  - text: parish's compasses
    url: http://sync.abue.io/issues/190705ap_sync2_27_compasses.pdf
---
Allison Parish's Compasses is a collection of poems that explores how words occupy space. In Compasses, the insertion of a word onto the page imposes a phonetic topography; words blend and spill into eachother like rising hills on an empty plane. Semantic Compasses is an attempt to match the idea, but explores the latent space of words on a semantic level - mapping North/South and East/West poles to units of meaning, rather than sound. Rather than publishing a collection of poems, I chose to release a tool that allow users to compose their own.