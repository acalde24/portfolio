---
layout: post
title: "all is all poetry"
date:   2018-01-08 17:41:39 -0700
text: default
links:
  - text: Github
    url: https://github.com/alex-calderwood/all_is_all_poetry
---
During my internship at ProductionPro Technologies, I concurrently worked on a model-agnostic framework for poetry generation. 

The framework is a basic instruction on how to use language models like Markov chains and variational auto-encoders for poetry.