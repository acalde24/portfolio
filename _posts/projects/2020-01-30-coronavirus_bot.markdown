---
layout: post
title: "coronavirus bot"
date:   2020-01-30 17:41:39 -0700
categories: jekyll update
text: covid
links:
  - text: Twitter
    url: https://twitter.com/coronavirus_bot
  - text: Harvard Technology Review
    url: https://harvardtechnologyreview.com/2020/05/17/covid-19-art/
---
The morning after researchers released the Coronavirus genome, I whipped together this bot to tweet it out, base-pair by base-pair. The genome is really short (like 24,000 base-pairs!) so I figured it would take about 17 days to tweet the whole thing out, and why not do just that?
