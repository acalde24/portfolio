---
layout: post
title: "cognitive process model of writing support tools"
date:   2022-03-15 17:41:39 -0700
text: default
links:
  - text: A Review of Writing Support Tools Using a Cognitive Process Model of Writing
    url: https://aclanthology.org/2022.in2writing-1.2/
---
Coauthored with Katy Ilonka Gero for the first annual In2Writing workshop.