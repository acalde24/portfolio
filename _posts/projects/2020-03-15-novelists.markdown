---
layout: post
title: "how novelists use generative language models"
date:   2020-03-15 17:41:39 -0700
text: default
links:
  - text: How Novelists Use Generative Language Models
    url: http://ceur-ws.org/Vol-2848/HAI-GEN-Paper-3.pdf
---
We recruited professional novelists to use AI writing tools to augment their practice, curious to learn if they would discover ways to use cutting-edge generative text tools in ways that haven't been forseen by programmers. In addition to learning about how to design such systems, we found that AI models can be used to see one's writing in a new light, akin to what Viktor Shklovsky termed "defamiliarization". 
