---
layout: post
title: "disinformation"
date:   2020-01-30 17:41:39 -0700
date:   2021-01-19 17:41:39 -0700
text: default
links:
  - text: Computation + Journalism
    url: https://docs.google.com/presentation/d/1b5ZTp-nax6ckfZ8VWKw5-McUovpNEomVGpUcEhB59nY/edit?usp=sharing
  - text: Twitter
    url: https://twitter.com/HumbleWrench
  - text: New York Academy of Sciences
    url: .
---
Eric Bolton and I realized through a 2018 deep learning class we took together at Columbia University that future advents in large language models would have severe implications for the spread of disinformation. We presented our research on the systemic barriers in place to prevent the spread of automated disinformation at the 2021 Computation + Journalism Symposium.

High Quality Real-Time Structured Debate Generation
