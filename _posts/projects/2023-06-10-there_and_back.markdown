---
layout: post
title: "there and back again"
date:   2023-06-10 17:41:39 -0700
text: default
links:
  - text: Proceedings of AAAI '23
    url: https://ojs.aaai.org/index.php/AIIDE/article/view/27502
---
Story generators using language models offer the automatic production of highly fluent narrative content, but they are hard to control and understand, seizing creative tasks that many authors wish to perform themselves. On the other hand, planning-based story generators are highly controllable and easily understood but require story domains that must be laboriously crafted; further, they lack the capacity for fluent language generation. In this paper, we explore hybrid approaches that aim to bridge the gap between language models and narrative planners. First, we demonstrate that language models can be used to author narrative planning domains from natural language stories with minimal human intervention. Second, we explore the reverse, demonstrating that we can use logical story domains and plans to produce stories that respect the narrative commitments of the planner. In doing so, we aim to build a foundation for human-centric authoring tools that facilitate novel creative experiences.

Coauthored with Jack Kelly, Noah Wardrip-Fruin, and Michael Mateas