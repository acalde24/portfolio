---
layout: post
title: "the itself"
date:   2023-11-14 00:00:00 -0400
text: default
links:
    - text: The Ground Itself
    url: https://everestpipkin.itch.io/the-ground-itself
    - text: Schaharazade
    url: https://acalde24.gitlab.io/portfolio/2023/11/01/schaharazade.html
---
The Itself is a (work in progress) game developed while searching for word production strategies for Schaharazade, my dadaist poetry insrument.

The project is borne of my failure to find a writing game that gives player actions that closely correspond to the creative decisions that poets and writers actually face. These are problems of diction, imagery, and form, which are all present in the game.

I hope to release the game on itch.io as a pdf, taking inspiration from Everest Pipkin's remarkable storytelling game, The Ground Itself. 