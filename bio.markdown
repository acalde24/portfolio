---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: post
title: "about"
text: default
links:
  - text: github
    url: https://github.com/alex-calderwood
  - text: linkedin
    url: https://www.linkedin.com/in/alexcalderwood/
  - text: buy me a coffee
    url: https://www.buymeacoffee.com/alexcalderwood

---
I am currently a Ph.D. student at the Expressive Intelligence Studio at UC Santa Cruz.

Previously, I studied Natural Language Processing at Columbia University while also enrolled in a graduate journalism degree.

I have worked as a writer at CU's Brown Instutue for Media Innovation, where I covered new approaches to journalism. I've also published investigative journalism, designed games, and worked in news automation (turning data feeds into words).

I have too many hobbies! I love surfing and trailrunning. I'm also dabbling with installation art - interactive language art, as well as metalwork. 

I am a writer! I am currently trying to get some poetry published. 
