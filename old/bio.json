{
	"Name": "Alex Calderwood",
	"Education": {
			"UC Santa Cruz": "Ph.D. Computational Media (Expected 2026)",
			"Columbia University": "M.S. Computer Science, Natural Language Processing (2019)",
			"Montana State University": "B.S. Computer Science (2017), Minors: English Literature and Math",
	},
	"Academic Publications": [
		{
			"title": "Spinning Coherent Interactive Fiction through Foundation Model Prompts",
			"url": "https://computationalcreativity.net/iccc22/wp-content/uploads/2022/06/ICCC-2022_2L_Calderwood-et-al..pdf",
			"authors": ["Alex Calderwood", "Noah Wardrip-Fruin", "Michael Mateas"],
			"year": "2022",
			"venue": "ICCC",
			"venue_long": "International Conference on Computational Creativity",
			"pubtype": "full"
		},
		{
			"title": "A Review of Writing Support Tools Using a Cognitive Process Model of Writing",
			"url": "https://aclanthology.org/2022.in2writing-1.2/",
			"authors": ["Katy Ilonka Gero", "Alex Calderwood", "Charlotte Li", "Lydia Chilton"],
			"year": "2022",
			"venue": "In2Writing",
			"venue_long": "ACL Workshop on Intelligent and Interactive Writing Assistants",
			"pubtype": "workshop"
		},
		{
			"Title": "How Novelists Use Generative Language Models: An Exploratory User Study",
			"Authors": "Alex Calderwood, Katy Gero, Vivian Qiu, Lydia Chilton",
			"Date": "March 15, 2020",
			"In": "HAI-GEN Workshop Paper - IUI 2020",
			"Link": "http://ceur-ws.org/Vol-2848/HAI-GEN-Paper-3.pdf"
		},
		{
			"Title": "High Quality Real-Time Structured Debate Generation",
			"Authors": "Niles Christensen, Eric Bolton, Alex Calderwood, Iddo Drori",
			"Date": "March 13, 2020",
			"In": "New York Academy of Sciences 2020 ML Symposium",
			"Link": "http://www.alexcalderwood.blog/papers/High-Quality-Real-Time-Structured-Debate-Generation.pdf"
		},
		{
			"Title": "Understanding the Semantics of Narratives of Interpersonal Violence through Reader Annotations and Physiological Reactions",
			"Authors": "Alexander Calderwood, Elizabeth Pruett, Raymond Ptucha, Christopher Homan, Cecilia Ovesdotter Alm.",
			"Date": "April, 4 2017",
			"Link": "http://www.cse.unt.edu/sembear2017/book.pdf"
		},
		{
			"Title": "Applying domain-specific natural language understanding techniques to film",
			"Author": "Alex Calderwood",
			"Date": "May 2017",
			"Link": "https://scholarworks.montana.edu/xmlui/handle/1/12904"
		}
	],
	"Employment": [
			{
				"Organization": "Bloomberg LP",
				"Role": "News Automation Engineer (Senior Software Engineer)",
				"Dates": "June 2020 - May 2021",
				"Involvement":
					"Transformed data streams and market signals into human-readable news articles. Wrote code responsible for thousands of such stories, working closely with journalists."
			},
			{
				"Organization": "Brown Institute for Media Innovation",
				"Role": "Postgraduate Research Scholar",
				"Dates": "May 2019 - March 2020",
				"Involvement": [
					"Provided technical assistance for a number of innovative journalism and new media projects.",
					"Wrote about the Brown Institute 'Magic Grants', working with reporters and technologists at the Brown Institute and publications such as The New York Times and ProPublica.",
					"Designed and led a workshop on intermediate Web Scraping techniques including DOM manipulation with Selenium."
				]
			},
			{
				"Organization": "ProductionPro Technologies",
				"Role": "Data Engineering Consultant",
				"Dates": "May 2018 - Aug 2019",
				"Involvement": [
					"Conducted research and development for an NLP system to parse unstructured film and theater scripts at Disney-backed startup.", 
					"Extracted information such as characters, scenes, locations, and props with semantic role analysis and frame extraction.", 
					"Scraped 1000's of film/theater script archives by developing a Python codebase for concurrent web crawling.",
					"Developed a tool to track semantic changes or 'diff' film and theater scripts using the Zhang-Shasha tree edit-distance algorithm. Adapted the concept from academic literature, and incorporated string-edit (Levenshtein) distance into the zss library."
				]
			},
			{
				"Organization": "Columbia University",
				"Role": "Teaching Assistant",
				"Dates": "May 2018 - May 2019",
				"Courses": ["Artificial Intelligence", "UI Design / Responsible CS Curriculum Development"]
			},
			{
				"Organization": "Rochester Institute of Technology",
				"Role": "Student Researcher",
				"Dates": "May 2016 - Aug 2016",
				"Involvement": [
					"Devised a method to apply machine learning-based computational linguistic analysis techniques to Reddit discussions of domestic violence. Presented the technique at the EACL conference in Valencia, Spain.", 
					"Lead author on an NSF funded research study, working with an interdisciplinary team of linguists and sociologists."
				]
			},
			{
				"Organization": "Montana State University",
				"Role": "Teaching Assistant",
				"Dates": "Aug 2015 - May 2017",
				"Courses": ["Introduction to Programming - Java", 
							"Programming with C", 
							"Texts and Critics Honors Seminar"]
			},
			{
				"Organization": "Oracle",
				"Role": "Software Engineer Intern",
				"Dates": "May 2015 - Jan 2016",
				"Involvement": "Developed a tool to streamline code review and save engineers time. Automated QA app deployment with shell scripts."
			}
	],
	"Journalism": [{
		"Publisher": "Wired Magazine",
		"Title": "How Americans Wound Up on Twitter's List of Russian Bots",
		"Link": "https://www.wired.com/story/how-americans-wound-up-on-twitters-list-of-russian-bots/",
		"Dates": "May 2018 - July 2018",
		"Description": "Employed computational analysis to discover errors committed by Twitter. Interviewed Twitter executives & Congressional aides, and published a story involving users whose accounts were incorrectly flagged by Twitter and identified by Congress as foreign agents."
	},
	{
		"Publisher": "Brown Institue - Columbia University",
		"Title": "Magic Grant Profile Series",
		"Link": "https://brown.columbia.edu/tag/mg-profiles/",
		"Dates": "February 2019 - April 2020",
		"Description": "Interviewed dozens of researchers (journalists, artists, and technologists) for a series of profiles about projects at the intersection of technology and media."
	}],
	"Links": {
		"Twitter": "https://twitter.com/alex_calderwoo",
		"Github": "https://github.com/alex-calderwood",
		"Linkedin": "https://www.linkedin.com/in/alexcalderwood/",
		"Email": "alexander.d.calderwood@gmail.com",
		"Website": "http://www.alexcalderwood.blog/",
		"Buy Me a Coffee": "https://www.buymeacoffee.com/alexcalderwood"
	}
}