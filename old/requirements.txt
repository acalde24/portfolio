gunicorn
Flask==2.0.3
Flask-Markdown
psycopg2-binary
sqlalchemy==1.3.2
Flask-SQLAlchemy
# ==2.4.4
# 1.3.24
dateparser
flask-heroku
pronouncing
flask_wtf
# Flask-Mobility
flask-pagedown



# in_two_dimensions requirements
# dill
# numpy
# scikit-learn
# gensim
