# 10:04
## January 31, 2019

Scratch a physicist and you’ll reveal a mystic

they say. In this sense, the author is a fractal magnifier, 

a wide-netted memory device, 

a transcriber of the rote whimsy of finely ordered details and micro-optics

They are a telescope into the empirical essence of upward-down celestial objectivity.

Uniformity makes way to highly intelligent unexplainable fortune 

in the form of opals dotting a black aqueous mass, biologic and suspect.

To inspect is to describe—by the Buddha’s teaching

Then refining is reflecting... like a seabed reflects 

and is refined I guess