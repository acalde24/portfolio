# 116th & Amsterdam
## March 18, 2018 

I wake into the stains of Manhattan,

worry persisting that one of the corner-people

has beaten me to being me

and that they'd done it better,

trooped into rather than trod onto

the breakneck curb.

 

Hear the siren folded in New York's lovers' streets,

the boys-in-blue

tapdancing to it their tepid beat?

Cameras draw. Hitchcock Presents;

A love song in black and white,

the pain of a noir murder-

romance that lacks a beating heart or a strong

female lead.

 

Allusions swarm around the block,

casting their shadows on my tourist's

suggestible gaze. Read the signs. 

Where am I now?

 

116th & Amsterdam. An ark

open to some few from

the tempest's huddled masses.

Restless city, your sheets drape a plight unknown.

This quilt haunts me. Your eyes crackle with light

and I can't afford your rent. 

 

On the corner now I've become the everyman

I can't ever really be.

"Jacques!" I call

and I wake at night.


