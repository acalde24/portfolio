# 2 Poems
## 2017

## Abstract Algebra

In abstraction lies something unwholesome,

and I must admit I think there's something

strange about the place a mind 

goes when it drifts away from the where

from the when 

&emsp;&emsp;&emsp;&emsp;                            to the what

is not known

&emsp;&emsp;&emsp;&emsp;                         but logical

and

&emsp;        intractable 

&emsp;&emsp;&emsp;                             and surreal.


## Klein's Dilemma

Homology, sacred geometry--

Topology spins all forms to orders

of rings and sings shape into beauty; with it

strings become knots and knots become numbers,

tangled wars of unsubstance and rumor.


I have trypophobia, and so do most--

the fear of holes that erupt where they shouldn't.

But mostly I'm afraid of getting lost 

in the gaps math closes coldly in logic, to be

moored in shores not anthropomorphic.

What of the voice that sings "Daisy, Daisy?" 

or "Open the pod bay doors."

                                           Says, "I will"?

What of Jean-Louis and his great diversion;

his night at Maud's and women are cosmic

obelisks enough to near the closing curtain.


It whispers, "Choose life! I chose something else."

And warns of a sign above Plato's cave,

"Let no one ignorant of geometry

enter", it said. So now I'm lost in a hall

that bends against logic impossibly.