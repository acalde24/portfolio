# The beach does not consist of all of it
## April 12, 2021

The beach does not consist of all of it,

in lapse or suspension, indeed saltation

(latin <i>saltus</i>: "leap") transports particles by creep

and delivers sediment over washing weeks

many winds push these waves and pebbles

as tides nudged gently by many moons

as the dolphin swims - as the seabird dives

the tortoise counts the tides - the surfer nurses her limbs

warring ocean crustaceans eye the next bigger dome

<i>dust to dust and crab to carbinate</i>

<i>the counted tides do not atone</i>

for wasted hours at borders, at lapping shores

and the conspiracy of cartographers gets obscured

(measuring each coast more exactly,

the lines now set and defined)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    by falling sand

but landform morphs, mangroves fill in

bluff bandit sandspits creep 

lighthouses grow

with indivisible grains the moments spill ashore

fruiting spores algae growth and coconuts provide

fringes boundaries or brims, sacrosanct, calcified

and the beach continues to not consist of all of it

at the places that the horizon begins 

to pull the lustful eye or asks the mouth to name ocean, sky

nor should you while while you breathe

always there at the coast while it may yet erode

nor let its representation consume 

that which waves do not linger on

allow the narrative outside the dingy

to catch, sway gently, bob and resume