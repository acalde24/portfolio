# Dido's Passage
## December 30, 2020

do you know how much time I've wasted

in this enchanted mire, beneath the harpies' lair

where water once flowed, settles still and naked
<br>
I allowed myself to sink to a place I thought sacred

as algae growth greens my billowing hair

do you know how much time I've wasted
<br>
there is still truth here, beneath it all, frustrated

signed in kelp our wedlock, marks of care

where water once flowed, settled still and naked

<br>

I can't help knowing now, I've seen baited

doves from out the sky, and in your snare 

do you know how much time I've wasted?

<br>

I've waged war over less, seen lust sated

on either side that Trojan despair

where water once flowed, settles still and naked

<br>

Gaze into Dido's eyes, you'll know me jaded

as you reach to Italy and the upper air

know how much time I've wasted

where water once flowed, settles still and naked