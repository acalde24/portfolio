# A sonnet for - from Gala
## January 21, 2022

*Suspended*, suspension, that’s what the critic\[[who?](https://en.wikipedia.org/wiki/Dream_Caused_by_the_Flight_of_a_Bee_Around_a_Pomegranate_a_Second_Before_Awakening)\] 

describes, to say nothing of the pomegranate

to say nothing of the obelisk, the elephant atop

the sky. The tigers orange on mid afternoon sunset

blue on Catalan waterdrop (cry) to have cried

and lifted the object to have \[         \]

sexualized the perceiver and to have therefore

perceived her in an afternoon disturbing the interpretation

otherwise by her the birth of the birth the 

cliffside next to \_\_\_\_\_\_\_\_  would have but didn’t

rise. *Rise.* Rise. By whom and for what and dropping

the subject modify that special hegemony

signified. Rise and suspended lie.

Post source and pour forth tree and rhizome fly