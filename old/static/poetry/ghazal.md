# A Ghazal for New Crobozon
## February 20, 2020


Seep, seeping. It is free, freeing

to reside within this city, where half-forms fracture


notions of simply being one-thing

All here are half things. Conspiring for more. Norms fractured.


as though each were in the process of unfolding.

An undoing chrysalis. Take the sky in swarms. Fracture.


Larvae nest here. Slums retake the gilded whalebone

And if not insectile, it’s at least somewhat vile, storms fracturing


undue stillness with police conspiracy, re-announced raids,

making arrests though some are guilty. ‘Order' transforms fracture


with an expectation; an abject tranquility

Enfolded in a subjective perversion; chains inform fracture.


All the air, our collective re-becoming is somehow still

as though this transformation locks us in place; the remade, reformed fracture.


Silent, silencing. Form, forming. Shackle, shackling.

Banal excised, we breathe together, and weather the horrid deformed fracture. 