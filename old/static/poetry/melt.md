# Melt
## June 23, 2018

Did I ever tell you I was scared of you?

This is why I didn't take the kiss

in your apartment, on the night

you called me your best friend in J-school. 

Doubt you remember and

I don't think it was true, the stress

on 'best' was wrong and besides,

what's wrong with you?

You're not cold, but you want

to be ice in a glass of wine,

a glacier in summer

anywhere it doesn't belong.
