# Modalities of a Whisper
## February 13, 2018

_See me_

An image. White sand.

Speckled oases nested 

in corneal flare.

Flecks of form in a marble bust,

(a moment, a shutter, a glance of hope)

and the rippling-red artist's curtain withdrawn.

Is it her?

A likeness maybe but nothing more.

With my photos develops an awareness

squinting--I don't share Pygmalion's skill.

 

_Hear me_

A cosmic standing wave. Resonations

from beyond some interstellar plane

perfused into the moan of a whale,

an aurora's breach, the creak

of a falling tree,

the unsung background radiation of

the observable seas. I will capture her

in the moments that deprecate sight

and coronate beauty in sound.

I tune my forks and I spend all day

looking for a voice in the clouds, cursing

the course timbre of my haggard calls

and wondering if not here,

where she will be.

 

_Find me_

A horizon. Sails.

Out of photonic waves and into

real ones. If she's just an image,

I'll put her to sea.

I'll seek the breath

of travel and charter

heat, sweat, and strain. 

I'll go nowhere once,

berate the monks

and the tillermen,

and plead them 

for her.