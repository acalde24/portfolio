# A Simple (The Seventh) Moral Tale
## April 22, 2022

I met a genie in a Turkish bazaar

who gave me wishes, said to arrange them far

and to reach through winding halls and tapestry

out of this country, out of harm and to see

and clasp for the root of all I can desire

nevermore to want to guess hunger or tire

I thanked him, the gleam in his eye and its plan

knew that for this gift, I was the perfect man

I could gift warmth and peace and prosperous soil

I could give to myself the absence of toil

but I’d read this tale and I knew of its end

so I thanked him and banked it, and then I said

I will take your gift and I’ll treat it as such

you have granted me permission not to rush


<i>Author's Note:</i> I recently watched [3000 Years of Longing](https://www.imdb.com/title/tt9198364/), which references the solution that I seem to be arguing for in this poem to the problem of interacting with a genie. The solution being, broadly: don't rush, don't make a wish at all. The movie seemed to grapple with the ramifications to the genie that this solution presents. Not to spoil it, I can't recommend it enough. 