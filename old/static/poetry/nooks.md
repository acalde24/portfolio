# Nooks
## June 2020

My friends are the nooks

between spaces filled

with hidden meaning

our jokes the riddles

left by our passing

the marks: vines and books

treasures hid as silent

as the waves they watch

the cities built around stairs

cobble but just one missing

notes tucked into cracks

gardens flung where soil lands

corners beckoning the sunlight left

we go right beyond to crooks

find each other in these nooks