# Thoughts on a skip
## Jan 28, 2020

Time turns out to be nothing but heat,

the average line of many samples the universe 

spouting foamy vibrations at the lowest levels disorder

doing the work of reconciling the statistical probability of motion

along one weak axis.

Science marches past the undergraduates

In their hands carbon-rust essence crumbles. They grow up

lawyers to tumble in wave-form

collapsed with their doubles. I want

none of this. I want to live forward the farm hands

my fathers and mothers. I want to observe the

sea in mid-dream. I want to repel the field of push-and-pull

the hiring forces breezing leaves an autumn eddy

like flotsam drifting along. I want to peak past the waterfall,

look up at the riverbed, down at the footfall and know

just once I have escaped the middling inevitability of the horizon-line

for the tug of oar, the dull bump bump of painted oak on gyroscoping prow.