# Yet
## July 12, 2018

A discordia,

an ill-transmittance

a bang in intermittent space

indeterminate,

undefined

and as of yet unordered



withering,

withdrawn

as though it’s been retracted

a passing thought passed-over

a promise of space contracted

 

nothing

not-yet

we’re content to wait

for forms indelible

remain inaudible—happenstance drawn to create