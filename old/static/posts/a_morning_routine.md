# A Morning Routine
## Jan 29, 2021

You buy a coffee from your girlfriend, who folds a heart into the foamy cream. You mistake the   symmetry of the cups as though they were one framed by opposing mirrors, but it sits beside many others just like it.

You meet her on the subway, hold her hand. She takes a selfie for the two of you, which projects onto the billboard above the train, unseen. 

Outside the frame, your uncle Seymour is there, and your girlfriend is too. Many versions of her, usually but not always accompanied by various versions of you. You make a quick joke to her as you arrive at your stop, brakes squealing, sparking steel. 

She whispers something and gives you a kiss. She is excited about her new job. She feels the world turning underneath with you.

You get off, she stays on the train, but it doesn't move. Someone has jumped onto the tracks and the train ahead. Seymour, you know it was Seymour who jumped. 

In the pre-work hustle, you'd think the people in the station would find any reason to be annoyed when the train runs slow. They aren't now. Everyone pulls together and is silent for a while. After you'd been gone for ten minutes, the train picks up again. As she gets off, she catches the gaze of Seymour, who gives her a little half smile, some pain in his eyes, a copy of the paper tucked in his arm and a coffee in the other. The number of Seymour suicides is up this year. 

She enters her building. You are working at the desk in the lobby. She asks if anything arrived for her in the mail. It has. When you hand over the package, her hand lingers on your own. You exchange a quiet moment, a lingering look, a kiss. She smiles at you. 

She enters an elevator, where you are going up. You mention to her that you're still waiting for a report that stakeholders have requested. "We need it quickly," you say. All you've talked about all week is the report. She shuffles slightly to the side of the elevator, looks at the door. "It's almost ready." She says curtly. When the elevator stops at your floor you get off. Right as the door begins to close she steps forward, sticks her arm through and gently grabs your forearm near the wrist. "I love you," she says. She means everything to you and you lean in to hug her in response.