# ATTAAAGGTT... and so forth
## January 31, 2020

There are lots of opinions about the right way that the media should treat public health scares like the Coronavirus, with critics of the current media attention to the virus rightfully arguing that due to low mortality and infection rates relative to other prominent contagions, the hype has been overblown **edit: it hasn't been, I wrote this in January!**. But I also want to point out that to mobilize the kind of response we’re seeing to the virus, you need this level of public awareness; and that’s certainly a result of the media frenzy. Overdue panic aside, I think this is a great example of media attention used correctly.

I read an article yesterday that said that the genome <a href=" https://www.ncbi.nlm.nih.gov/nuccore/MN908947">was published 20 days prior</a>, and I thought that was incredible. From my high school Biotechnology class, I remember how difficult it was to develop gene sequencing technology for the human genome. High throughput polymerase chain reaction machines are only about a decade old, and now we routinely sequence new viruses the moment they are discovered. What a time to be alive.

Anyway, when I read about this, I had to do what any researcher would and make a Twitter Bot to publish the genome, base-pair by base-pair. It should take about 16.6 days to tweet the whole thing out because the Wuhan Coronavirus is *remarkably small*, only about 24,000 base-pairs. That’s insane. And how much of that DNA actually codes for anything useful? Not much. Nature is efficient.

Check out the progress of the bot here: <a href="https://twitter.com/coronavirus_bot">https://twitter.com/coronavirus_bot</a>
