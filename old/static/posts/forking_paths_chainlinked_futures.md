# Forking Paths, Chainlinked Futures
## Oct 2, 2021

*A response to The New Media Reader, chapters 1-8 (Borges, Bush, Turing, Weiner, Linklider, Boroughs)*

Reading these passages, we act half as historians, and half as futurists. At first it was (beautifully) unexpected to be exposed to Vannevar Bush’s (still unrealized) vision of mechanical thought trails (the memex) alongside Borges’ exploration of the structural parallel between branching fiction and a multiverse cosmology. But all technologies are ideas. Systems of meter and methods of organizing thought. Boroughs’ “cut-up”, the notion of a ‘Happening’. Weiner’s concept of negative feedback as a means of control. These are confluences of past ideas, scaffolding together towards our future technologies. 

Turing observed a fallacy — “the assumption that as soon as a fact is presented to a mind all consequences of that fact spring into the mind simultaneously with it”. Technologies also, do not spring up. I don’t think I’ve realized until now how proximate we are to the birth of these grand technological ideas, or how nascent our use of them is. 

The connection between our world and this recent past is less than one link away in the scaffold of recombinant technologies. It seems that we still haven’t hammered out all of the tools that will allow us to realize the visions presented by theorists of the mid 20th century. That leaves us with two problems: spinning the web of possible futures (to figure out what is worth building), and following it up with the hard mechanical scaffolds of realized technology.
