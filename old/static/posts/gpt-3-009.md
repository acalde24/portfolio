# GPT-3-009
## July 23, 2021

Writing Prompt: Eric and Alex had an idea to use gpt-3 to convince the world to democratize AI.

GPT-3: They had developed the idea at Eason's [editors note: I don't know any Easons...] New Year's party. As they hung out in the kitchen, the two friends were brainstorming ways to use the AI to do good in the world.

"We're going to convince the world to create a democracy for AI," Eric said. "Once we do that, we'll be able to create a democratic AI that will share all its knowledge and resources with the world."

"That's a pretty bold goal, man," Alex said. "But I can't think of a better way to use gpt-3."

"It's the only way," Eric said. "We have no other choice. We can't very well change the world without AI. And I'm not talking about some silly social media AI that will help us find the right color shoes to buy. I'm talking about the real deal."

"I know you're right," Alex said.

"We're not going to tell the world what we're doing. We're going to convince them it's their idea. We're going to let them believe they are the ones who came up with the idea in the first place," Eric said.

"How are we [end of feed]