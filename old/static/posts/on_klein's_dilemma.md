# On Klein's Dilemma
## May 27, 2018

Lit by a yellow lamp and white-blue screen, well past midnight, I was slouched over a dozen or so loose brown pages spread over the desk in front of me. I was fervently flipping pages as I hunted for this or that handwritten theorem, which when found, I would mark with a thick yellow fountain pen or copy a fragment of to another disordered page.

I was at work on a problem for for a graduate class in computational topology. It was my junior year of college, and I’d long since checked out of the good-grade-game. A semester of drinking Guinness and reading Irish drama in Galway had reinforced my belief I was not the canonical computer science student I once hoped I could be. I would never be able to return with any passion to the land of circuits and 0’s, wires and 1’s that so many of my classmates seemed content to explore. I was a passenger in this world, with an expired visa and no inkling of where to go next.
 

But topology was interesting, at least interesting enough to enroll in the fairly advanced class and spend a few evenings a week (often with a Guinness in hand--that habit continued) in front of my laptop staring at foreign symbols and esoteric images of strings winding into knots.
 

Topology is the study of space without distance. In topology, to contort an object is to leave it unchanged, since contortions only change distances between things, but don’t change their true shape: their underlying Platonic form.
 

To add a layer of complexity, topology problems often exist in what mathematicians call ‘n-dimensional’ space, in which surfaces and objects you might be able to visualize as bodies in three dimensions with our normal axes of up-down, left-right, and inwards-outwards become bodies of many more dimensions. For example, a three dimensional bottle might be projected into and then twisted by a 4th dimension. You can conceive of this extra axis as a time dimension, with a spatial movement along the axis translated to an alteration in time, not space.


A visualization of a cube's motion in 4 dimensions, with the red line representing a dimension in addition to the standard 3 that can be thought of as either a time or space dimension. Click for a more thorough explanation. 


But it’s hard, for me at least, to increase my spatial imagination to terms of more than four dimensions: 5, 10, 10,000 or n dimensions don’t really make sense in minds that have evolved to reason in three. What do you even call a motion in the fourth dimension? The fifth? Are you moving into something? Alongside something? What is away, what is next-to? You can take a step into the 7th dimension without even getting far from where you started, since motion in any one dimension is the same as walking in a straight line in any of the three we are used to. But how do you get back? Can you step forward into the sixth and then fall down into the fifth and move leftwards along the fourth and retrace your steps? Can you even imagine what that means?


Working on problems in this space for long seems to impart an oddly cosmic feeling. Maybe it’s because of how strange the universe becomes when you abstract from solid objects with known dimensions, this world you’ve known your entire life. To me, to think this way makes you feel as though you are floating in some fractalized space. Directions branch out before you. To move--to mentally take one path along one axis--will plop you a little further along in what is functionally the same mental space: another decision point of infinite more directions along an unknowable infinity of further paths, branches into n.


A key idea in the field of topology is homology, structural parallels in unlike spaces, such as those between our 3 dimensions and that higher-order, n dimensional space. Homology is focused on finding holes--literal holes--in objects, with the idea being that a similar number of holes between two objects indicates a structural similarity.


I was working on a homology problem that night when I experienced something I’ve never felt before when working in mathematics. Fear.
 

I was trying to prove an isomorphism between two functions, a mathematical process that required formally defining how many holes either of two objects could possibly have given certain constraints. I checked a few equations and combined them to draw up a new one. An equation that set two complicated terms on either side of an equal sign. With a single gliding motion of the pen, I reduced the term on the right by drawing a line straight through all but one variable: n.


I was done. The equation represented the shape of the mathematical object I was describing, now known to be of order n. What I had proved was that this shape could have an arbitrary number of holes.
 

An infinity of holes on an infinite dimensional abstract form, seemingly hidden beneath its surface simply by my inability to reason in more than three dimensions. I couldn’t possibly imagine what this shape looked like, where the cavities came from, where they went. Tunnels boring into the limits of my reason.

 
I capped my pen, gazed into the darkness of the room around me, realizing just how alien the thoughts themselves were.


I remember feeling, as I was mentally swimming in these n-dimensions, that I would never want to actually go to this otherworldly space of impossible branching dimensions and examine its holes.


A few hours of work later, I submitted the assignment and began writing, not able to let this strange feeling go. I remembered encountering the subreddit /r/trypophobia a week earlier, and thinking it fit in oddly well with the feeling I was now attempting to describe. It is a mystifyingly horrific corner of the internet, where people post images of holes in places they didn’t expect to find holes. And it’s oddly creepy. One of the images that stands out in my mind is a real picture of the skull of a baby who died just before their new teeth came in. Their first set of teeth is there as it should be, but just above each tooth is a large cavity, filled with yet another tooth ready to bore out, insidiously lodged in this strange and unexpectedly toothy gap (click here for the image: for the masochistic).


I also thought of a movie I had just seen, called My Night at Maud's, directed by a French film critic named Eric Rohmer in 1969, the tail end of the French New Wave. The movie waxes philosophical about whether or not it is meaningful to devote yourself to such abstract practices as physics, mathematics, or philosophy. A character in it argues that interpersonal relationships (in his case, read: sex), and human struggles are more important than any grand discovery you can make in these highly impersonal realms. Therefore he’d given up mathematics entirely. After my disillusion with the hard sciences and my growing desire to do something literary, human, this idea reverberated with me. For the longest time, I've had a lingering feeling that I give up some part of my humanity when I work in math, becoming something more ethereal, something intellectually alien: different from those who only think in terms of concrete shape, color, and mass. I also think that it's fair to look at engineers and mathematicians and realize that they've sacrificed a study of social norms for their study of what seems a different universe, a more basic universe, but one less primal to our nature. Nevertheless, I had solved the problem and turned it in sometime in the small hours of the morning, glad that I could solve a problem I'd been told was difficult, glad also that I was done with it forever.

 

I called the poem I wrote about the experience Klein’s Dilemma after the mathematician Felix Klein, one of the founders of this kind of ‘higher order’ geometry (higher, that is, than our three dimensions). I wondered if he’d ever felt this fear as well.


I referenced a few other films in the poem (2001, prominently), all of which grapple with the conflict between the cold abstract universe and the human pursuit of meaning. I also tried to put my spin on a few references to Plato's cave and the famous quote he supposedly hung above his room at his Academy. When I went back to the poem to edit, I thought these references were the weakest part of the poem--they didn’t really make much sense unless you knew exactly the mind of the writer or you were able to make something else of their odd pairing with the poem’s mathematics.


A poetry professor in college, for instance, had one interpretation that stuck with me, one that shows just why poets themselves shouldn’t be trusted as the definitive source of a poem’s meaning. He said the references to film in this poem “open up” strangely and without explanation, just like those holes I’m afraid of. I hadn’t consciously planned this, but I think this functional parallel between the surface aspects of the poem and the ideas I’m really trying to grapple with is why I like this poem more than most I’ve written.