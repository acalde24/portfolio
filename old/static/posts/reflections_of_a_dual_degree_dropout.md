# Reflections of a Dual Degree Dropout
## July 27, 2018

I’ve always wanted to drop out of something, and now that I have I’m kind of sad that it’s all over. In so many ways, last year was the most intense of my life, vacillating between a deep-seated imposter’s syndrome and moments of brand-new clarity, utter reward. I was a member of Columbia’s fifth or sixth cohort of the Dual Degree in Journalism and Computer Science, a program that costs an estimated $210,000 over the two years (before scholarships), factoring in the cost of living in NYC. I want this post to address some of my thoughts about the program: where it shines, and why I left.



Little has been written about the degree since some initial buzz in 2010, and few of its graduates have weighed in online, save for a few scattered Reddit and Quora posts. Since the degree is meant to prepare students for a career in either Journalism or Computer Science, they take the entire computer science course-load as well as the entire journalism course-load, five semesters of classes crunched into a grueling four. Students are co-registered in Columbia’s School of Engineering and Applied Sciences as well as the School of Journalism, known to its students as the J-School.



The program was conceived as a Frankenstein Hail Mary from a troubled industry. It was meant to breathe a bit of life into the school, at a time when Facebook and Google are drawing most of the advertising money away from traditional media outlets, away from, “an able, disinterested, public-spirited press.” At least that’s how it was pitched. In our acceptance letter, dual-degree students are told:


> Our four-semester academic program will further enhance your training in computer science and journalism, enabling you to create and refine new news gathering and digital media technologies that will redefine journalism as we know it.


It’s a clever sales pitch.



The price tag is the first thing that sticks out to most people, who wonder how you could justify spending so much for a degree in the relatively low-paying field of journalism. The answer is a bit complicated. For one, every student in the program who applied for aid last year was partially funded by scholarships from donors to the J-School, in varying amounts. And while most journalism jobs have a salary lower than the yearly tuition of this school, the dual-degree students seem to place into well-paying careers, with some graduates going on to work at tech behemoths like Google, established outlets like The Wall Street Journal, and various new-media startups.



In reality, few of the students are here to make money. They come to this specific program with varying degrees of disenchantment about tech and the lifestyle of coders. In order to get in to the program you have to demonstrate competence in coding, so they’ve experienced this lifestyle before. They come to the program as multi-talented malcontents, often without clear direction.



Speaking broadly, the CS courses don’t address the needs of these students. As a CS master’s degree, it is incredibly restrictive, compared to most graduate CS degrees of this caliber in the country. It requires that students choose from a limited number of lower level ‘breadth’ courses to fill a large number of their allotted classes (such as introductory Artificial Intelligence, Software Engineering, or Databases). I’m told that this is a restriction of the New York state government. It requires that to gain accreditation, new graduate programs must propose a set of classes that hold to the state regulatory standards and the school must enforce this list with little flexibility for students who want to sidestep the requirements for classes more appropriate to their future field. And though they cite these state requirements, other CS degrees, such as the Natural Language Processing-track master’s degree at Columbia that I am transferring to, are far less restrictive in the classes in which you are allowed to enroll.



For this reason, the program is not well suited for students that come in with an undergraduate degree in CS—they will find themselves repeating classes they’ve already taken. Nor it is well suited for students that come in with a specific CS interest not directly related to journalism—they’ll still have to take a number of unrelated breadth courses.



The website that advertises the program, which enticed me to apply one dark December evening, slouched between stale pizza boxes and a recent breakup, makes it seem that it is trying to draw creative-technologists with a focus on the humanities, storytelling, a deep social drive.



In the classes, I found little creativity. We spent most of the first semester torn between beat-reporting—totally removed from our domain-knowledge—and a single computer science class that was so divorced from the rest of our days that it was impossible to concentrate on. I was only happy when I was away from school, hanging out with newfound friends (I’ve met some of the most interesting, passionate, and driven people I’ve ever known while at this school) or exploring the city on foot by myself, surrounded by its horrifying grandeur. I was aware that at every moment my thoughts might get drawn to the inescapable reality that I’d made a mistake in coming here, in shackling myself to a colossal debt for an opportunity to come to a dream school that I never could have turned down.



The J-School, where dual-degree students spend most of their time and do most of their socializing, consists of a strange amalgam of people. On the one hand, it is an Ivy League school, and all of the students there are there because in one way or another, they can afford to be. The school, I’ve found, espouses social progressiveness but is entrenched in a pervasive conservatism, a sense that certain rules must be followed for order to be maintained. Like the heavy stone the school is built of, and the concrete that holds it in place, things do not give way when pushed. The metal bust of Joseph Pulitzer, the school’s founder, that sits in the atrium, or the statue of Jefferson just outside, remind all who enter that history will be remembered here, that tradition is our guide. On the first day of the required class “The Business of Journalism”, I remember the words of Pulitzer projected on the screen, detailing his desire to never teach business at this school, only craft. His rebellion at once contradicted and sacrosanct.



The professors seem yesterday’s rebels, tenured rebels. At the end of the day, many are encouraging to new thoughts, and it’s the structure of classes itself, the narrow bead of what is allowed under the school and its assignment’s strict guidelines, that seems to frustrate so many of the students. Open mindedness is encouraged only so far as it can be controlled. A female dual-degree that started a working group to discuss computational journalism was only allowed to go so far until a professor took it over, dominating it by his ideas, his voice.



Some of the journalism students buy into the structure. They are all here because they can afford to accelerate their careers in this way. Yet, they all want to be journalists, exposing truth, holding the powerful to account, with social-mindedness, and expression, and ego and all. I’ve found these students to be full of optimism and a drive to make the world better, but at this wealthy school I’ve found few that are truly radical, fewer yet that pursue journalism for the love of knowledge. At every turn, I’ve felt that information is a tool. When the scoop translates to immediate reward—recognition and return—it has to be. It’s a currency here. I found myself yearning for an artistry in it, a poetry.



I think the program, with close affiliation with the Brown Institute and the Tow Center for Digital Journalism, is trying to position itself as a journalistic equivalent of the MIT Media Lab in Boston or the Internet and Telecommunications Program (ITP) here in New York. These are places that take cross-disciplinarily seriously, encouraging rigorous cross pollination between computing, and social sciences or humanities. A few stellar faculty at the school makes it seem that it’s succeeding. Susan McGregor, Jonathan Albright, and Mark Hansen are some of the biggest names in computational journalism right now, and I think they are transforming the entire discipline at a time that serious transformations need to occur.



These institutions, the Tow Center and Brown Institute, fascinate me. They are what drew me here. The more I am exposed to their resources, the incredible insight of their faculty and staff, the more optimistic I get about the very future of technology. Whenever I hear people throw in the towel, exclaim that our screen’s dark mirror or our social spiderweb is going to consume us, I think of all of the exceptional work that’s being done to warn us of technology’s pitfalls. I think that if everyone was a bit more tuned into the work these people are doing, the more they’d rest assured that the Cambridge Analyticas, the NSA’s, and the even the Googles of world can be fought, can be defeated.



The program attracted some of us for our contrarian nature: programmers that could have had a long (and better paying) career building things, unleashing our creations on the world’s consumers, who decided to go a road less trod, one standing a bit more in the face of the present.



I admire what institutions like these are trying to do, a thing so new that it is hard to give it a name. You hear about privacy advocacy, about computational journalism, digital journalism, digital humanities, and you hear descriptors thrown around such as creative-technologist, or a new one that I like, digital humanitarian, to describe the people in and around these new media institutions. No description is quite adequate. Some of the people in this field, people I’ve just recently been exposed to while at Columbia, are nothing short of prophets. I look at people like Mark Hansen at the Brown Institute and others like Glenn Greenwald, or the reporter Carole Cadwalladr, people that live in the world of Snowden, but are less upset about it. I see people that are living a reality at once both distant and near, future and now, intuiting problems that because of their foresight, we may now avoid.


I want to study in and around spaces like these. More than anything, I want to be in a truly free creative space where I can learn to observe like these people observe, where I can learn to contribute like they have. I admire the other dual-degree students who have chosen to stay. Each is working at the intersection of media and technology, doing work that excites me. I expect big things from all of them.



But I am leaving. Though the ideas excite me, and the program as it was sold is exactly the thing I want to do, for whatever reason I just don’t vibe with it. Maybe I’m too contrarian, maybe in some way, my desire to drop out is just an anti-authoritarian angst that I would have felt anywhere. All told, I feel like I got a lot out of the year I spent at the J-School, and I know I’ll be able to leverage some of what I learned far into the future. I also know to expect brilliant things from those that have stayed, and those that are to enter institutions like these.


