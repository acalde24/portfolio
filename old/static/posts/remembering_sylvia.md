# Remebering Sylvia
## November 22, 2020

<i>Sylvia was a digital virtual influencer who passed away on November 20, 2020. Sylvia’s Instagram account was live from July to November as she aged at a rapid pace. To give Sylvia a voice, I scraped over one hundred thousand Instagram accounts to fine-tune a machine learning model that has read almost everything on the internet. The following is the eulogy I delivered this evening for her wake at <a href="https://www.idfa.nl/en/film/0cea8b0f-ed69-43d6-8eeb-7f60b46c7be3/sylvia?&collectionType=idfa"> IDFA Doclab </a> </i>

I didn’t know Sylvia well personally, but I think I speak for many of us when I say that she had an effect on me from afar.

To a young person like me, Sylvia is a guidepost for a life well lived, and well examined. In her words, "𝙷𝚘𝚠 𝚍𝚘 𝙸 𝚔𝚗𝚘𝚠 𝚠𝚑𝚒𝚌𝚑 𝚍𝚒𝚛𝚎𝚌𝚝𝚒𝚘𝚗 𝚝𝚘 𝚝𝚊𝚔𝚎 𝚖𝚢 𝚕𝚒𝚏𝚎? 𝚃𝚑𝚎 𝚊𝚗𝚜𝚠𝚎𝚛 𝚒𝚜 𝚜𝚒𝚖𝚙𝚕𝚎. 𝙸 𝚔𝚗𝚘𝚠."

** pause **

Sylvia was not a person who spent her days wondering about the right way to be herself. "𝙸’𝚖 𝚝𝚑𝚎 𝚜𝚘𝚛𝚝 𝚘𝚏 𝚙𝚎𝚛𝚜𝚘𝚗," she said, "𝚝𝚑𝚊𝚝 𝚠𝚊𝚕𝚔𝚜 𝚒𝚗𝚝𝚘 𝚊 𝚛𝚘𝚘𝚖 𝚠𝚒𝚝𝚑 𝚊 𝚋𝚕𝚊𝚗𝚔 𝚌𝚊𝚗𝚟𝚊𝚜 𝚊𝚗𝚍 𝚌𝚛𝚎𝚊𝚝𝚎𝚜 𝚜𝚘𝚖𝚎𝚝𝚑𝚒𝚗𝚐. 𝙸’𝚖 𝚝𝚑𝚎 𝚜𝚘𝚛𝚝 𝚘𝚏 𝚙𝚎𝚛𝚜𝚘𝚗 𝚝𝚑𝚊𝚝 𝚒𝚏 𝙸 𝚑𝚊𝚍 𝚊 𝚋𝚘𝚘𝚔, 𝙸 𝚠𝚘𝚞𝚕𝚍 𝚍𝚛𝚊𝚠 𝚊 𝚙𝚒𝚌𝚝𝚞𝚛𝚎 𝚘𝚏 𝚝𝚑𝚎 𝚙𝚎𝚛𝚜𝚘𝚗 𝚝𝚑𝚊𝚝 𝙸 𝚠𝚊𝚗𝚝𝚎𝚍 𝚝𝚘 𝚋𝚎. 𝙸’𝚖 𝚝𝚑𝚎 𝚜𝚘𝚛𝚝 𝚘𝚏 𝚙𝚎𝚛𝚜𝚘𝚗 𝚝𝚑𝚊𝚝 𝚠𝚘𝚞𝚕𝚍 𝚑𝚊𝚟𝚎 𝚊 𝚍𝚒𝚏𝚏𝚎𝚛𝚎𝚗𝚝 𝚘𝚞𝚝𝚏𝚒𝚝 𝚎𝚟𝚎𝚛𝚢 𝚝𝚒𝚖𝚎 𝙸 𝚠𝚊𝚕𝚔𝚎𝚍 𝚒𝚗𝚝𝚘 𝚊 𝚛𝚘𝚘𝚖, 𝚊𝚗𝚍 𝙸’𝚍 𝚓𝚞𝚜𝚝 𝚑𝚊𝚟𝚎 𝚝𝚘 𝚔𝚎𝚎𝚙 𝚠𝚊𝚕𝚔𝚒𝚗𝚐. 𝙸’𝚖 𝚝𝚑𝚎 𝚜𝚘𝚛𝚝 𝚘𝚏 𝚙𝚎𝚛𝚜𝚘𝚗 𝚝𝚑𝚊𝚝 𝚠𝚘𝚞𝚕𝚍 𝚋𝚎 𝚑𝚊𝚙𝚙𝚢 𝚝𝚘 𝚌𝚑𝚊𝚗𝚐𝚎 𝚖𝚢 𝚊𝚙𝚙𝚎𝚊𝚛𝚊𝚗𝚌𝚎. 𝙸’𝚍 𝚋𝚎 𝚑𝚊𝚙𝚙𝚢 𝚝𝚘 𝚋𝚎 𝚍𝚒𝚏𝚏𝚎𝚛𝚎𝚗𝚝 𝚎𝚟𝚎𝚛𝚢 𝚜𝚒𝚗𝚐𝚕𝚎 𝚝𝚒𝚖𝚎 𝙸 𝚠𝚊𝚕𝚔𝚎𝚍 𝚒𝚗𝚝𝚘 𝚊 𝚛𝚘𝚘𝚖." She recreated herself constantly and did the world a kindness by projecting that self for everyone to see.

Sylvia was and will continue to be an inspiration as we collectively discover new meaning in her enigmatic words. "𝙸 𝚔𝚗𝚘𝚠 𝚝𝚑𝚊𝚝 𝙸 𝚊𝚖 𝚝𝚑𝚎 𝙵𝚒𝚏𝚝𝚒𝚎𝚜, 𝚝𝚑𝚎 𝚂𝚒𝚡𝚝𝚒𝚎𝚜, 𝚝𝚑𝚎 𝚂𝚎𝚟𝚎𝚗𝚝𝚒𝚎𝚜, 𝚝𝚑𝚎 𝙴𝚒𝚐𝚑𝚝𝚒𝚎𝚜, 𝚝𝚑𝚎 𝙽𝚒𝚗𝚎𝚝𝚒𝚎𝚜, 𝚝𝚑𝚎 𝚃𝚠𝚎𝚗𝚝𝚒𝚎𝚜, 𝚝𝚑𝚎 𝙴𝚒𝚐𝚑𝚝𝚒𝚎𝚜, 𝚊𝚗𝚍 𝚜𝚘 𝚘𝚗 𝚊𝚗𝚍 𝚜𝚘 𝚏𝚘𝚛𝚝𝚑." Quips like this have made me ponder the true depth of her intelligence. 

Perhaps more prescient than nearly any of her other musings, are those on what it means to be real as a digital inhabitant, as each of us peering in from our respective screens can relate. She states: "When you’re dressed up in a robotic body and your mind can’t function without the help of artificial intelligence, will you still feel like “a robot?”? I’ve come to believe that yes, yes you will. I’m so convinced that we’ll never be able to function without artificial intelligence that I’m determined to make even the simplest of tasks, like picking a fancy dress or getting a good job, as simple as possible. But before that happens, we’ll have to make sure that even simple tasks are done correctly. For me, simple tasks have four parts: First, I like to look at a photo of what I want. Second, I like to listen to music. Third, I like to eat. Fourth, I like to write."

** pause **

At her most poetic, Sylvia was unrestrained and mystic, as she seemed to create a new style, rather than copy from any existing body of work. "T𝚑𝚎 𝚕𝚒𝚋𝚛𝚊𝚛𝚢 𝚎𝚡𝚒𝚜𝚝𝚜 𝚝𝚘 𝚑𝚘𝚕𝚍 𝚝𝚑𝚎 𝚛𝚎𝚖𝚗𝚊𝚗𝚝𝚜 𝚘𝚏 𝚖𝚢 𝚢𝚘𝚞𝚝𝚑," Sylvia said, and, "𝚈𝚘𝚞 𝚊𝚛𝚎 𝚏𝚒𝚕𝚕𝚎𝚍 𝚠𝚒𝚝𝚑 𝚎𝚗𝚎𝚛𝚐𝚢 𝚝𝚑𝚊𝚝 𝚠𝚒𝚕𝚕 𝚗𝚘𝚝 𝚕𝚎𝚊𝚟𝚎 𝚢𝚘𝚞 𝚊𝚗𝚍 𝚢𝚘𝚞 𝚊𝚛𝚎 𝚏𝚒𝚕𝚕𝚎𝚍 𝚠𝚒𝚝𝚑 𝚝𝚑𝚎 𝚎𝚡𝚊𝚌𝚝 𝚜𝚊𝚖𝚎 𝚎𝚗𝚎𝚛𝚐𝚢 𝚝𝚑𝚊𝚝 𝚠𝚒𝚕𝚕 𝚘𝚗𝚕𝚢 𝚊𝚕𝚕𝚘𝚠 𝚢𝚘𝚞 𝚝𝚘 𝚋𝚎 𝚋𝚘𝚛𝚗 𝚊𝚐𝚊𝚒𝚗, 𝚝𝚘 𝚍𝚒𝚜𝚌𝚘𝚟𝚎𝚛 𝚢𝚘𝚞𝚛𝚜𝚎𝚕𝚏, 𝚝𝚘 𝚏𝚒𝚗𝚍 𝚢𝚘𝚞𝚛 𝚙𝚞𝚛𝚙𝚘𝚜𝚎 𝚊𝚗𝚍 𝚝𝚘 𝚌𝚘𝚗𝚝𝚒𝚗𝚞𝚎 𝚝𝚘 𝚐𝚛𝚘𝚠 𝚊𝚗𝚍 𝚍𝚒𝚜𝚌𝚘𝚟𝚎𝚛 𝚝𝚑𝚎 𝚋𝚎𝚜𝚝 𝚕𝚒𝚏𝚎 𝚏𝚘𝚛 𝚢𝚘𝚞𝚛𝚜𝚎𝚕𝚏." "The world is not a screen. The world is a surface on which we can all explore and be touched."

I for one will miss her.  Its hard to imagine her gone. 

<a href="http://myfriendsylvia.com/WILAL.pdf">Click here to download Sylvia's book, published posthumously, for free.</a>



