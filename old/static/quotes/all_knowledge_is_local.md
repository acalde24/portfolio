<i>
“Lines and colors made with earth on earth may hold knowledge in them. All knowledge is local, all truth is partial,” Havzhiva said with an easy, colloquial dignity that he knew was an imitation of his mother, the Heir of the Sun, talking to foreign merchants. “No truth can make another truth untrue. All knowledge is a part of the whole knowledge. A true line, a true color. Once you have seen the larger patttern, you cannot go back to seeing the part as the whole."</i>
 
<br>
\- Ursula K. Le Guin

[A Man of the People](https://en.wikipedia.org/wiki/A_Man_of_the_People_(short_story))