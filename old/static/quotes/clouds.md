<i>"Who can leap the world's ties and sit with me among white clouds?"</i>
 
<br>
\- Jack Kerouac

[The Dharma Bums](https://www.goodreads.com/work/quotes/827497)
