<i>I moved with the ease of a psychiatrist who becomes fond of his patients, enjoying the balmy breezes that waft from the ancient park of his private clinic. After a while he begins to write pages on delirium, then pages of delirium, unaware that his sick people have seduced him. He thinks he has become an artist.</i>
 
<br>
\- Umberto Eco

[Foucault's Pendulum](https://www.hjkeen.net/halqn/fpendlm2.htm)