<i>
“Looking at the surf near the spit at the beach, you’ll notice that certain water patterns recur over and over—perhaps a double-crowned wave on the right, perhaps a bubbling pool of surge beside the rock, perhaps a high-flown spray of spume off the front of the rock."</i>
 
<br>
\- Rudy Rucker

[Surfing the Gnarl](https://books.google.com/books?id=HCOR7Y-Pze4C&pg=PA50&lpg=PA50&dq=surfing+the+gnarly+rudy+rucker)