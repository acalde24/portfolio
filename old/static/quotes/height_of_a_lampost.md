<i>The city does not consist of this, but of relationships between the measurements of its space and the events of its past: 
the height of a lamppost and the distance from the ground of a hanged usurper's swaying feet; 
the line strung from the lamppost to the railing opposite and the festoons that decorate the course of the queen's nuptial procession; ...</i>
 
<br>
\- Italo Calvino

[Invisible Cities](http://www.cittainvisibili.com/en/portfolio/zaira-incisione-en.html)
