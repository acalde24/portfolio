<i>I must Create a System, or be enslav’d by another Man’s;

I will not Reason and Compare: my business is to Create.</i>
 
<br>
\- William Blake

[Jerusalem](https://www.bartleby.com/235/307.html)
