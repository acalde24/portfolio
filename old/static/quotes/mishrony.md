<i> They say here “all roads lead to Mishnory.” To be sure, if you turn your back on Mishnory and walk away from it, you are still on the Mishnory road. To oppose vulgarity is inevitably to be vulgar. You must go somewhere else; you must have another goal; then you walk a different road. … To be an atheist is to maintain God. </i>

<br>
\- Ursula K. Le Guin

The Left Hand of Darkness