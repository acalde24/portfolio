<i>
"In the evening she looked again. They were crossing a golden sea, in which lay many small islands and one peninsula. She repeated, ‘No ideas here,’ and hid Greece behind a metal blind."
</i>
 
<br>
\- E.M. Forster

[The Machine Stops](https://www.ele.uri.edu/faculty/vetter/Other-stuff/The-Machine-Stops.pdf)