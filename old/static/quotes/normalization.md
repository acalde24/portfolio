<i>The unwillingness to tolerate the condition of <bold>un</bold>knowing in which we all live may lie behind the ancient and persistent tendency to believe that some powerful force controls all aspects of our lives -- a poewr in other words, writing a story that will eventually become clear. Some apply this at the political level to sinister forces ,like the government or international conspiracies. But in fact this has long been, and continues to be, a very common way of viewing the universe itself. In his 1979 study of the interpretation of narrative Frank Kermod puts this point strongly:

> If there is one belief (however the facts resist it) that unites us all, from the evangelists to those who argue away incovenient portions of their texts and those who spin large plots to accomodate the discrepencies and dissonances into some larger scheme, it is this conviction that somewhow, in some occult fasion, if we could only detect it, everything will be found to hang together. (Genesis of Secrecy, 72)
</i>
 
<br>
\- H. Porter Abbot, on the power of narrative to 'normalize' beliefs -- to make any fact believable due to its context within a causally-structured narrative

The Cambridge Introduction to Narrative