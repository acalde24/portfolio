<i>
“Some of the dream-sages wrote gorgeously of the wonders beyond the irrepassable gate, but others told of horror and disappointment. I knew not which to believe, yet longed more and more to cross forever into the unknown land; for doubt and secrecy are the lure of lures, and no new horror can be more terrible than the daily torture of the commonplace. So when I learned of the drug which would unlock the gate and drive me through, I resolved to take it when next I awaked."</i>
 
<br>
\- H.P. Lovecraft

[Ex Oblivione](https://www.hplovecraft.com/writings/texts/fiction/eo.aspx)