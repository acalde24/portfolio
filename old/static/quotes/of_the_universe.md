<i>My mother was of the sky

My father was of the earth

But I am of the universe</i>

and you know what that's worth.

<br>
\- John Lennon and Paul McCartney

[Yer Blues](https://www.youtube.com/watch?v=qXD7qY_xQHc)