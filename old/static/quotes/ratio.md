<i>

The Spectre is the Reasoning Power in Man, & when separated

From Imagination and closing itself as in steel in a Ratio

Of the Things of Memory, It thence frames Laws & Moralities

To destroy Imagination, the Divine Body, by Martyrdoms & Wars.

</i>
 
<br>
\- William Blake

[Jerusalem](https://www.bartleby.com/235/307.html)
