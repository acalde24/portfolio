<i>The salt in the small bowl looks up at me

with all its little glittering eyes and says:

I am the dry sea.

Your blood tastes of me.</i>

<br>
\- Ursula K. Le Guin

[Late in the Day](https://books.google.com/books?id=akvYCgAAQBAJ&pg=PT39&lpg=PT39&dq=I+am+the+dried+sea+your+blood+tastes+of+me++ursula+le+guin#v=onepage&q=I%20am%20the%20dried%20sea%20your%20blood%20tastes%20of%20me%20%20ursula%20le%20guin)