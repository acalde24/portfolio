<i>Music, states of happiness, mythology, faces belabored by time, certain twilights and certain places try to tell us something, or have said something we should not have missed, or are about to say something; this imminence of a revelation which does not occur is, perhaps, the aesthetic phenomenon. 
</i>
 
<br>
\- Jorge Luis Borges

[Everything and Nothing](https://www.goodreads.com/en/book/show/17945)