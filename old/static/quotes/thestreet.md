<i>
The street is, in this sense, the alternative and subversive form of the mass media, since it isn't, like the latter, an objectified support for answerless messages, a transmission system at a distance. It is the frayed space of the symbolic exchange of speech—ephemeral, moral: a speech that is not reflected on the Platonic screen of the media. Institutionalized by reproduction, reduced to a spectacle, this speech is expiring. 
</i>
 
<br>
\- Jean Baudrillard

[Requiem for the Media](http://shmacek.faculty.noctrl.edu/Courses/MediaCritSyllabusSPR2_files/19-baudrillard-03.pdf)
