<i>
"Although we take writing so much for granted as to forget that it is a technology, writing is in a way the most drastic of the three technologies of the word. It initiated what printing and electronics only continued, the physical reduction of dynamic sound to quiescent space, the separation of the word from the living present, where alone real, spoken words exist."</i>
 
<br>
\- Walter J. Ong

[Writing Is a Technology That Restructures Thought](https://w3.ric.edu/faculty/rpotter/temp/ong.pdf)
