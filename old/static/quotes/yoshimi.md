<i>
Those evil-natured robots

They're programmed to destroy us

She's gotta be strong to fight them

So she's taking lots of vitamins

<br>

'Cause she knows that 

It'd be tragic

If those evil robots win
</i>
 
<br>
\- The Flaming Lips

[Yoshimi Battles The Pink Robots Part 1](https://open.spotify.com/track/0ccCwNzXvr1Yoz91vKz31Z?si=ce27c6a82a4d4ff9)
